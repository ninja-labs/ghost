import Vue from 'vue';
import App from './App.vue';
import router from './router';
import './registerServiceWorker';
import store from './store';
import { initAuth } from 'feathers-vuex';
import VueTouch from 'vue-touch'
import VueMeta from 'vue-meta'
import * as Sentry from '@sentry/browser'
import * as Integrations from '@sentry/integrations';
import Overdrive from 'vue-overdrive'
import { log, timeout, color } from '@/mixins'
import BButton from '@c/bbutton'
import NavigationBar from '@c/navigation/Bar'
import NavigationButton from '@c/navigation/Button'
import 'jquery'
import 'jquery-mousewheel'
import VueTextareaAutosize from 'vue-textarea-autosize'

if(window.location.href.includes('ref=makerads')) {
  window.location.href = 'https://todoninja.de/?ref=makerads'
}

if(process.env.NODE_ENV == 'production') {
  Sentry.init({
    dsn: 'https://6db4330efa41410caf5254f116d6a162@sentry.io/1545910',
    integrations: [new Integrations.Vue({ Vue, attachProps: true })],
    environment: process.env.NODE_ENV,
    release: JSON.parse(unescape(process.env.PACKAGE_JSON || '%7Bversion%3A0%7D')).version
  })
}

Vue.use(VueTouch)
Vue.use(Overdrive)
Vue.use(VueMeta)
Vue.use(VueTextareaAutosize)

Vue.mixin(log)
Vue.mixin(timeout)
Vue.mixin(color)

Vue.component('bbutton', BButton)
Vue.component('navigation-bar', NavigationBar)
Vue.component('navigation-button', NavigationButton)

Vue.config.productionTip = false;

window.vue = new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');

window.Vue = Vue
window.initAuth = initAuth;
window.store = store;